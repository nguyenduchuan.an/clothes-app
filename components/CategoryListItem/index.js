import React from 'react'
import { View, StyleSheet, Image, Text } from 'react-native'
import ShoesImage from '../../assets/images/shoes.jpg'

const CategoryListItem = () => {
    return (
        <View>
            <Text>Item</Text>
            <Image source={ShoesImage} style={styles.imageStyle} />
        </View>
    )
}

export default CategoryListItem

const styles = StyleSheet.create({
    imageStyle : {
        width: 100,
        height: 100
    }
})