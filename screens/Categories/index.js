import React from 'react'
import { View } from 'react-native'

import CategoryListItem from './../../components/CategoryListItem'

const Categories = () => {
    return (
        <View>
             <CategoryListItem />
        </View>
    )
}

export default Categories
